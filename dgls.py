import numpy as np
import scipy.integrate
import matplotlib.pyplot as plt

def slopesegments(f, x0=-1., xmax=1., y0=-1., ymax=1., grid=20, seg=0.7, deq=False):

    xr = np.linspace(x0,xmax,grid+1)
    yr = np.linspace(y0,ymax,grid+1)
    
    xgrid = xr[1]-xr[0]
    ygrid = yr[1]-yr[0]

    fig = plt.figure()

    for x in xr[1:-1]:
        for y in yr[1:-1]:
            dx = 1
            dy = f(y,x)
            l = np.sqrt((dx/xgrid)**2 + (dy/ygrid)**2)
            dxp = dx * seg / l
            dyp = dy * seg / l
            plt.plot([x-dxp/2,x+dxp/2],[y-dyp/2,y+dyp/2],'b')

    if deq:
        T = np.linspace(x0,xmax,200)
        for yinit in yr[1:-1]:
            Y = scipy.integrate.odeint(f,yinit,T)
            plt.plot(T,Y,'r')

        plt.axis([x0,xmax,y0,ymax])

    plt.draw()

    return fig
