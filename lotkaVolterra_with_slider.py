import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
from scipy.integrate import ode

def LV(t,x,r1,r2,C1,C2):
    prey = x[0]
    predator = x[1]
    dprey = r1 * prey - C1 * predator * prey
    dpredator = C2 * predator * prey - r2 * predator
    return np.array([dprey, dpredator])


tmax_sim = 100
tmax_plot = 30

ymax_plot = 2.5

deltat = 0.1

y0 = 1
r2 = 1
C2 = 1


def sim(x0, fn, *args):
    r = ode(fn)
    #print(args)
    r.set_initial_value(np.array(x0)).set_f_params(*args)
    Ylist = [r.y]
    Tlist = [r.t]
    while r.successful() and r.t<tmax_sim:
        Ylist.append(r.integrate(r.t+deltat))
        Tlist.append(r.t)
    Y = np.array(Ylist)
    T = np.array(Tlist)

    return T, Y


slider_height = 0.05
slider_gap = 0.15

slider_data = {
    'r1': [0,2,0.66,0.01],
    'C1': [0,2,1.33,0.01],
    'x0': [0,2,1,0.01]
    }

vals = {}
for k,v in slider_data.items():
    vals[k] = v[2]


fig, (ax1, ax2) = plt.subplots(1,2)
plt.subplots_adjust(left=0.1, right=0.9, bottom=len(slider_data)*slider_height + slider_gap)

T,Y = sim([vals['x0'],y0], LV, vals['r1'], r2, vals['C1'], C2)

l1,l2 = ax1.plot(T, Y)
ax1.legend(['prey','predator'])
ax1.set_xlim(0,tmax_plot)
ax1.set_ylim(0,ymax_plot)
ax1.margins(x=0)
ax1.set_xlabel('time')

llist = [l1,l2]

#ax2.subplots_adjust(left=0.55, right=0.9, bottom=len(slider_data)*slider_height + slider_gap)

l3, = ax2.plot(Y[:,0],Y[:,1])
ax2.set_xlim(0,ymax_plot)
ax2.set_ylim(0,ymax_plot)
ax2.set_xlabel('prey')
ax2.set_ylabel('predator')
#llist.append(l3)

axcolor = 'lightgoldenrodyellow'
axlist = []
sliders = {}
y = slider_height
for k,v in slider_data.items():
    ax = plt.axes([0.25,y,0.65,slider_height*0.6], facecolor=axcolor)
    axlist.append(ax)
    y += slider_height

    sliders[k] = Slider(ax, k, v[0], v[1], valinit=v[2], valstep=v[3])

def update(val):
    for k,slider in sliders.items():
        vals[k] = slider.val
    T,Y = sim([vals['x0'],y0], LV, vals['r1'], r2, vals['C1'], C2)

    for i in range(len(llist)):
        llist[i].set_xdata(T)
        llist[i].set_ydata(Y[:,i])
    
    l3.set_xdata(Y[:,0])
    l3.set_ydata(Y[:,1])
    
for slider in sliders.values():
    slider.on_changed(update)

resetax = plt.axes([0.1, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')

def reset(event):
    for slider in sliders.values():
        slider.reset()

button.on_clicked(reset)


plt.show()
