import modelbase
#import modelbase.model as model
import modelbase.ratelaws as rl
#import modelbase.algebraicModule as algModule

import numpy as np

import matplotlib.pyplot as plt

class ErythrocyteGlycolysisB(modelbase.Model):

    defaultPars = {'kHK_PFK': 3.2, # 1/h
                   'kP2GM': 1500., # 1/h
                   'kP2Gase': 0.15, # 1/h
                   'kPGK': 1.57e4, # 1/(mM h)
                   'kPK': 559., # 1/(mM h)
                   'kATPase': 1.46, # 1/h
                   'nH': 4.,
                   'KI_ATP': 1., # mM
                   'qAK': 2., # mM
                   'A': 1.5 #mM
    }

    altPar = {'kHK_PFK': 2.29, # 1/h
              'nH': 1.
    }

    stdConc = {'G13P': .0005, # all concentrations in mM
               'G23P': 5.,
               'PEP': 0.02,
               'Ap': 1.124
    }


    @staticmethod
    def adenosine_phosphates(par, y):
        Ap = y[0]
        q = par.qAK
        A = par.A

        ADP = A*(-q/(4-q) + np.sqrt(q**2/((4-q)**2)+q*(1-(Ap/A)**2)/(4-q)))
        AMP = (A - ADP - Ap)/2.
        ATP = A - ADP - AMP

        return np.array([ATP,ADP,AMP])

    @staticmethod
    def vHK_PFK(p,A3):
        f = 1/(1+(A3/p.KI_ATP)**p.nH)
        return p.kHK_PFK*A3*f

    @staticmethod
    def vP2GM(p,y):
        return p.kP2GM * y

    @staticmethod
    def vP2Gase(p,y):
        return p.kP2Gase * y

    @staticmethod
    def vPGK(p,y,z):
        return p.kPGK * y * z

    @staticmethod
    def vPK(p,y,z):
        return p.kPK * y * z

    @staticmethod
    def vATPase(p,A3):
        return p.kATPase * A3

    @classmethod
    def vPGKss(cls,p,Ap):
        AxP = cls.adenosine_phosphates(p,[Ap])
        p13g = 2*cls.vHK_PFK(p,AxP[0])/(p.kP2GM+p.kPGK*AxP[1])
        return p13g*p.kPGK*AxP[1]

    def __init__(self, pars = {}):
        
        defPars = ErythrocyteGlycolysisB.defaultPars
        pars.update(defPars)

        super(ErythrocyteGlycolysisB, self).__init__(pars)

        # add all independent variables
        self.add_cpds(list(ErythrocyteGlycolysisB.stdConc.keys()))

        # add equilibrium module for ADP, AMP
        #ap = algModule.AlgebraicModule({'A':self.par.A,'qAK':self.par.qAK}, self.adenosine_phosphates)
        self.add_algebraicModule(self.adenosine_phosphates, 'AxP' , ['Ap'], ['ATP','ADP','AMP'])

        # define rate equations for reactions

        # upper glycolysis HK_PFK
        self.set_rate('HK_PFK',self.vHK_PFK,'ATP')
        self.set_stoichiometry('HK_PFK',{'Ap':-2,'G13P':2})

        # Bisphosphoglycerate mutase P2GM
        self.set_rate('P2GM',self.vP2GM,'G13P')
        self.set_stoichiometry('P2GM',{'G13P':-1,'G23P':1})

        # P2Gase
        self.set_rate('P2Gase',self.vP2Gase,'G23P')
        self.set_stoichiometry('P2Gase',{'G23P':-1,'PEP':1})

        # PGK
        self.set_rate('PGK',self.vPGK,'G13P','ADP')
        self.set_stoichiometry('PGK',{'G13P':-1,'PEP':1,'Ap':1})

        # PK
        self.set_rate('PK',self.vPK,'PEP','ADP')
        self.set_stoichiometry('PK',{'PEP':-1,'Ap':1})

        # ATPase
        self.set_rate('ATPase',self.vATPase,'ATP')
        self.set_stoichiometry('ATPase',{'Ap':-1})



    def makeFigure54(self):
        p = self.par
        psave = p.__dict__.copy()
        apr = np.linspace(-p.A, p.A, 1000)
        axp = np.vstack([self.adenosine_phosphates(p,[apr[i]]) for i in range(len(apr))])
        vhk = self.vHK_PFK(p,axp[:,0])

        fig = plt.figure()
        #plt.rc('text', usetex=True)
        #plt.rc('font', family='serif')
        plt.plot(axp[:,0],vhk)

        p.update(ErythrocyteGlycolysisB.altPar)
        vhk = self.vHK_PFK(p,axp[:,0])
        plt.plot(axp[:,0],vhk)

        plt.legend(["n="+str(psave['nH']),"n="+str(p.nH)])
    
        plt.xlabel("ATP (mM)")
        plt.ylabel("v_{HK-PFK} (mM/h)")

        if plt.isinteractive():
            plt.draw()

        p.update(psave)
        return fig


    def makeFigure55(self):
        p = self.par
        apr = np.linspace(-p.A, p.A, 1000)
        axp = np.vstack([self.adenosine_phosphates(p,[apr[i]]) for i in range(len(apr))])

        fig = plt.figure()
        plt.plot(axp[:,0]/p.A,axp[:,1]/p.A)
        plt.plot(axp[:,0]/p.A,axp[:,2]/p.A)
        plt.legend(['ADP/A','AMP/A'])
        plt.xlabel('ATP/A')

        if plt.isinteractive():
            plt.draw()

        return fig


    def makeFigure56(self):
        p = self.par
        apr = np.linspace(-p.A, p.A, 1000)
        axp = np.vstack([self.adenosine_phosphates(p,[apr[i]]) for i in range(len(apr))])

        vpgk=np.array([self.vPGKss(p,apr[i]) for i in range(len(apr))])

        fig = plt.figure()
        plt.plot(axp[:,0],vpgk,linewidth=2,linestyle='dashed',color='black')
        kATPase=[5.83,4.23,1.46]
        legs = ['v_{PGK}']
        for i in range(len(kATPase)):
            plt.plot(axp[:,0],kATPase[i]*axp[:,0])
            legs.append('v_{ATPase}, k='+str(kATPase[i]))
        plt.legend(legs)
        plt.xlabel('ATP (mM)')
        plt.ylabel('rates (mM/h)')

        if plt.isinteractive():
            plt.draw()

        return fig
