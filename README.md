This repository contains models and code used in various of my lectures and tutorials on mathematical modelling of biological systems.

All code is published open source under the CC-BY license (see license.txt)

Detailed instructions how to use the models are given in the respective directories / files.
