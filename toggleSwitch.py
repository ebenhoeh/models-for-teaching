# python code for toggle switch

import numpy as np
import scipy.optimize

""" general activation term depending on
    u: max activation rate
    n: Hill coefficient
    I: inhibitor concentration
""" 
def activation(u,n,I):
    return u/(1+I**n)

    
def deq(z,t,a,b,n,m):
    x=z[0]
    y=z[1]
    dxdt = activation(a,n,y) - x
    dydt = activation(b,m,x) - y
    return (dxdt,dydt)

def toggleswitch_ode(t,u,a,b,n,m):
    dxdt = activation(a,n,u[1]) - u[0]
    dydt = activation(b,m,u[0]) - u[1]
    return np.array([dxdt,dydt])


def steadyStates(a,b,n,m):
    def f(x):
        return activation(a,n,activation(b,m,x)) - x

    res = []
    
    U = np.linspace(0,a,100)
    c = f(U)
    s = np.sign(c)
    for i in range(100-1):
        if s[i] + s[i+1] == 0:
            u = scipy.optimize.brentq(f, U[i], U[i+1])
            res.append(u)

    return res
