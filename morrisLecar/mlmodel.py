# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import modelbase
import numpy as np

class MLmodel(modelbase.Model):
    
    p = {'V1'  : -1.2,
         'V2'  : 18,
         'V3'  : 2,
         'V4'  : 30,
         'VK'  : -84,
         'VL'  : -60,
         'VCa' : 120,
         'gK'  : 8,
         'gL'  : 2,
         'gCa' : 4.4,
         'Cm'  : 20,
         'phi' : 0.04
         }

    def __init__(self):
        super().__init__(MLmodel.p)
        
    @staticmethod
    def gateinf(V,V1,V2):
        return 0.5 * (1 + np.tanh((V-V1)/V2))
    
    
    def currents(self,V,w):
        
        p = self.par

        Ica = p.gCa * self.gateinf(V,p.V1,p.V2) * (V - p.VCa)
        IK = p.gK * w * (V - p.VK)
        IL = p.gL * (V - p.VL)

        return (Ica, IK, IL)
    
    
    def nullcline_V(self,V):
        
        p = self.par
        
        (Ica, IK, IL) = self.currents(V,0)
        return - (Ica + IL) / (p.gK * (V-p.VK))
    
    def nullcline_w(self,V):
        
        p = self.par
        return self.gateinf(V,p.V3,p.V4)
        
    
    def model(self,y,t):
        
        p = self.par
        
        V = y[0]
        w = y[1]
        
        (Ica, IK, IL) = self.currents(V,w)    
        dVdt = (- Ica - IK - IL) / p.Cm
        
        tau = 1 / (np.cosh((V-p.V3)/(2*p.V4)))
        dwdt = p.phi * (self.gateinf(V,p.V3,p.V4) - w) / tau
        
        return np.array([dVdt, dwdt])

        
