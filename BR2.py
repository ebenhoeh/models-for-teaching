#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 14 22:30:25 2017

@author: oliver
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
plt.interactive('True')

import parameters

p = parameters.ParameterSet()

p.update({'A': 1,
          'k1': 2,
          'k2': 1,
          'k2M': 1,
          'k3': 1,
          'k3M': 0,
          'k4': 1,
          'k4M': 1,
          'k5': 1,
          'k5M': 0,
          'k6': 1,
          'k6M': 1
})

def BR_rates(y,p):
    X1 = y[0]
    X2 = y[1]
    X3 = y[2]
    B = y[3]
    C = y[4]
    
    v1 = p.k1*p.A
    v2 = p.k2*X1 - p.k2M*X2
    v3 = p.k3*X2 - p.k3M*B
    v4 = p.k4*X1 - p.k4M*X3
    v5 = p.k5*X3 - p.k5M*C
    v6 = p.k6*X3 - p.k6M*X2

    return np.array([v1,v2,v3,v4,v5,v6])

def BR_deq(y,t,p):

    v = BR_rates(y,p)
    dX1dt = v[0] - v[1] - v[3]
    dX2dt = v[1] - v[2] + v[5]
    dX3dt = v[3] - v[4] - v[5]
    dBdt = v[2]
    dCdt = v[4]
    
    dydt = np.array([dX1dt,dX2dt,dX3dt,dBdt,dCdt])
    
    return dydt

T = np.linspace(0,10,500)
y0 = np.zeros(5)
Y = odeint(BR_deq,y0,T,args=(p,))

plt.plot(T,Y[:,0:3])
plt.legend(['X1','X2','X3'])
plt.draw()

