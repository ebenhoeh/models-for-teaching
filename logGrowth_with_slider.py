"""
===========
Slider Demo
===========

Using the slider widget to control visual properties of your plot.

In this example, a slider is used to choose the frequency of a sine
wave. You can control many continuously-varying properties of your plot in
this way.
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
from scipy.integrate import ode

def logGrowth(t, u, r, K):
    x = u[0]
    dxdt = r*x*(1 - x/K)
    return np.array([dxdt])


x0 = 0.1
tmax = 100
deltat = 0.1
a = 1

def simlogGrowth(x0,gamma,K):
    r = ode(logGrowth)
    r.set_initial_value(np.array([x0])).set_f_params(gamma,K)
    Ylist = [r.y]
    Tlist = [r.t]
    while r.successful() and r.t<tmax:
        Ylist.append(r.integrate(r.t+deltat))
        Tlist.append(r.t)
    Y = np.array(Ylist)
    T = np.array(Tlist)

    return T, Y



fig, ax = plt.subplots()
plt.subplots_adjust(left=0.1, bottom=0.25)
t = np.arange(0.0, 1.0, 0.001)


r0 = 0.1
K0 = 10
x00 = 0.1
delta_r = 0.01
delta_x0 = 0.01
delta_K = 0.1


T, Y = simlogGrowth(x00,r0,K0)

l, = plt.plot(T, Y)
ax.set_xlim(0,100)
ax.set_ylim(0,11)
ax.margins(x=0)

axcolor = 'lightgoldenrodyellow'
axr = plt.axes([0.25, 0.05, 0.65, 0.03], facecolor=axcolor)
axx0 = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)
axK = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)
#axamp = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)

sliderr = Slider(axr, 'r', 0, 1, valinit=r0, valstep=delta_r)
sliderx0 = Slider(axx0, 'x0', 0, 10, valinit=x00, valstep=delta_x0)
sliderK = Slider(axK, 'K', 0, 20, valinit=K0, valstep=delta_K)
#samp = Slider(axamp, 'Amp', 0.1, 10.0, valinit=a0)


def update(val):
    r = sliderr.val
    x0 = sliderx0.val
    K = sliderK.val

    T, Y = simlogGrowth(x0,r,K)
    l.set_xdata(T)
    l.set_ydata(Y)
    fig.canvas.draw_idle()


sliderr.on_changed(update)
sliderx0.on_changed(update)
sliderK.on_changed(update)
#samp.on_changed(update)

resetax = plt.axes([0.1, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')


def reset(event):
    sliderr.reset()
    sliderx0.reset()
    sliderK.reset()
    #samp.reset()
button.on_clicked(reset)


plt.show()

