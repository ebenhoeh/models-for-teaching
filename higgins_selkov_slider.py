"""
===========
Slider Demo
===========

Using the slider widget to control visual properties of your plot.

In this example, a slider is used to choose the frequency of a sine
wave. You can control many continuously-varying properties of your plot in
this way.
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
from scipy.integrate import ode

def higginsselkov(t, u, k, v):
    x = u[0]
    y = u[1]
    dxdt = v - x*y*y
    dydt = x*y*y - k*y
    return np.array([dxdt,dydt])


#x0,y0 = 0.5,0.5
#x0=[0.5,0.5,2,2]
#y0=[0.5,2,0.5,2]
x0=[2]
y0=[2]
tmax = 300
deltat = 0.01


def simhs(x0,y0,k,v):
    r = ode(higginsselkov)
    r.set_initial_value(np.array([x0,y0])).set_f_params(k,v)
    Ylist = [r.y]
    Tlist = [r.t]
    while r.successful() and r.t<tmax:
        Ylist.append(r.integrate(r.t+deltat))
        Tlist.append(r.t)
    Y = np.array(Ylist)
    T = np.array(Tlist)

    return T, Y



fig, ax = plt.subplots()
plt.subplots_adjust(left=0.1, bottom=0.25)
t = np.arange(0.0, 1.0, 0.001)
k0 = 1
v0 = 1
delta_k = 0.01
delta_v = 0.01

lines = []
for i in range(len(x0)):
    T, Y = simhs(x0[i],y0[i],k0,v0)

    l, = plt.plot(Y[:,0], Y[:,1])
    lines.append(l)
    
ax.set_xlim(0,5)
ax.set_ylim(0,5)
ax.margins(x=0)

axcolor = 'lightgoldenrodyellow'
axv = plt.axes([0.1, 0.1, 0.8, 0.03], facecolor=axcolor)
axk = plt.axes([0.1, 0.15, 0.8, 0.03], facecolor=axcolor)

sliderv = Slider(axv, 'v', 0, 2, valinit=v0, valstep=delta_v)
sliderk = Slider(axk, 'k', 0, 2, valinit=k0, valstep=delta_k)


def update(val):
    v = sliderv.val
    k = sliderk.val
    for i in range(len(x0)):
        T, Y = simhs(x0[i],y0[i],k,v)
        lines[i].set_xdata(Y[:,0])
        lines[i].set_ydata(Y[:,1])
    fig.canvas.draw_idle()


sliderv.on_changed(update)
sliderk.on_changed(update)

"""
resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')


def reset(event):
    sliderb.reset()
    #samp.reset()
button.on_clicked(reset)

rax = plt.axes([0.025, 0.5, 0.15, 0.15], facecolor=axcolor)
radio = RadioButtons(rax, ('red', 'blue', 'green'), active=0)


def colorfunc(label):
    l.set_color(label)
    fig.canvas.draw_idle()
radio.on_clicked(colorfunc)

# Initialize plot with correct initial active value
colorfunc(radio.value_selected)
"""

plt.show()

