__author__ = 'oliver'

import numpy as np
import matplotlib.pyplot as plt
import math

def slopefield(fn, xmin=0., xmax=1., ymin=0., ymax=1., xsteps=10, ysteps=10, args = ()):
    '''
    plots a slope field for function fn(x,y,...)
    args are optional parameters to pass to fn
    '''

    fig = plt.figure()

    xlen = (xmax - xmin)/xsteps
    ylen = (ymax - ymin)/ysteps

    rx = 0.45 * xlen
    ry = 0.45 * ylen

    # r = 0.9*min(xlen,ylen)/2.

    for x in np.linspace(xmin,xmax,xsteps):
        for y in np.linspace(ymin,ymax,ysteps):
            dydx = fn(x,y,*args)
            px = 1./np.sqrt(1./(rx**2) + (dydx**2)/(ry**2))
            py = px * dydx
            plt.plot([x-px,x+px],[y-py,y+py],'b')

    plt.xlim(xmin - xlen/2, xmax + xlen/2)
    plt.ylim(ymin - ylen/2, ymax + ylen/2)


    return fig
