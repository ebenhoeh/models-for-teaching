#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 15 14:55:46 2017

@author: oliver
"""

import numpy as np
import matplotlib.pyplot as plt
import parameters

plt.interactive(True)

from scipy.integrate import odeint

# define parameters

par = {'kK1':0.,
       'kK2':0.,
       'kK3':0.,
       'kK4':0.,
       'kK567':0.,
       'kK8':0.,
       'kK8M':1.,
       'kT1':0.,
       'kT1M':0.,
       'kT2':0.,
       'kT2M':0.,
       'kT3':0.,
       'kT3M':0.,
       'kT5':0.,
       'kT5M':0.,
       'kT6':0.,
       'kL':0.,
       'kLM':0.,
       'kAS':0.,
       'kASM':0.,
       'kANT':0.,
       'kANTM':0.,
       'kG5':2.,
       'kG5M':2.,
       'kNN':0.,
       'kNNM':0.,
       'kRC':0.,
       'kATPASE':2.,
       'kG1':1.,
       'kPL1':0.,
       'kAA1':0.}
p = parameters.ParameterSet(par)

def rates(y,p):
    OAAm,ACoAm,CITm,AKGm,MALm,Pim,PYRm,Hm,ATPm,ATPc,PYRc,NADH2m,NADH2c,CITc,AKGc,PL,AA = y

    ADPm = 4-ATPm
    ADPc = 4-ATPc
    NADc = 4-NADH2c
    NADm = 4-NADH2m

    v = {'K1': p.kK1*PYRm*NADm,
         'K2': p.kK2*PYRm*ATPm,
         'K3': p.kK3*ACoAm*OAAm,
         'K4': p.kK4*CITm*NADm,
         'K567': p.kK567*AKGm*Pim*ADPm*NADm,
         'K8': p.kK8*MALm*NADm - p.kK8M*OAAm*NADH2m,
         'T1': p.kT1*CITm - p.kT1M*MALm,
         'T2': p.kT2*MALm - p.kT2M*AKGm,
         'T3': p.kT3*MALm - p.kT3M*Pim,
         'T5': p.kT5 - p.kT5M*Pim*Hm,
         'T6': p.kT6*PYRc,
         'L': p.kL - p.kLM*Hm,
         'AS': p.kAS*ADPm - p.kASM*ATPm*Hm,
         'ANT': p.kANT*ATPm*ADPc - p.kANTM*ATPc*ADPm,
         'G5': p.kG5*PYRc*NADH2c - p.kG5M*NADc,
         'NN': p.kNN*NADH2c*NADm - p.kNNM*NADH2m*NADc,
         'RC': p.kRC*NADH2m*Hm,
         'ATPASE': p.kATPASE*ATPc,
         'G1': p.kG1*NADc*ADPc,
         'PL1': p.kPL1*NADH2c*ATPc*CITc,
         'AA1': p.kAA1*AKGc*ATPc*NADH2c}
    
    return v

#init OAAm = 0.1
#init ACoAm = 1
#init CITm = 1
#init AKGm = 1
#init MALm = 1
#init Pim = 1
#init PYRm = 1
#init Hm = 1
#init ATPm = 1
#init ATPc = 1
#init NADH2c =1 
#init NADH2m =1 
#init CITc = 1
#init AKGc = 1
#init PYRc = 1
#init PL = 0
#init AA = 0
y0 = np.array([0.1,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,0.,0.])

def SK_deq(y,t,p):
    v = rates(y,p)
    
    dOAAm = v['K2'] + v['K8'] - v['K3']
    dACoAm = v['K1'] - v['K3']
    dCITm = v['K3'] -v['K4'] - v['T1']
    dAKGm = v['K4'] - v['K567'] + v['T2']
    dMALm = v['K567'] - v['K8'] + v['T1'] - v['T2'] - v['T3']
    dPim = v['K2'] -v['K567'] + v['T3'] + v['T5']
    dPYRm = v['T6'] - v['K1'] - v['K2']
    dHm = v['L']+3*v['AS']-10*v['RC']+v['T5']
    dATPm = -v['K2']+v['K567']+v['AS']-v['ANT']
    dATPc = 2*v['G1']+v['ANT']-v['ATPASE']-14*v['PL1']-2*v['AA1']
    dPYRc =2* v['G1']-v['G5']-v['T6']
    dNADH2m = v['NN']+v['K1']+v['K4']+v['K567']+v['K8']-v['RC']
    dNADH2c = 2*v['G1']-v['G5']-v['NN']-14*v['PL1']-2*v['AA1']
    dCITc = v['T1']-v['PL1']
    dAKGc = -v['T2']-v['AA1']
    dPL = v['PL1']
    dAA = v['AA1']

    dydt = np.array([dOAAm,
                     dACoAm,
                     dCITm,
                     dAKGm,
                     dMALm,
                     dPim,
                     dPYRm,
                     dHm,
                     dATPm,
                     dATPc,
                     dPYRc,
                     dNADH2m,
                     dNADH2c,
                     dCITc,
                     dAKGc,
                     dPL,
                     dAA
                    ])
    
    return dydt

T = np.linspace(0,20,500)

Y = odeint(SK_deq,y0,T,args=(p,))

plt.plot(T,Y)

