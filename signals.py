import modelbase.model as model
import modelbase.ratelaws as rl
import modelbase.algebraicModule as algModule

import numpy as np

import matplotlib.pyplot as plt

def timeMeasures(y,t):

    ydim=y.shape
    tmult = np.repeat(t.reshape((len(t),1)),ydim[1],axis=1)
    I = np.trapz(y, t, axis=0)
    T = np.trapz(y*tmult, t, axis=0)
    Q = np.trapz(y*tmult*tmult, t, axis=0)
    tau = T/I
    theta = np.sqrt(Q/I - tau**2)
    S = I/(2.*theta)

    return tau,theta,S,I,T,Q

