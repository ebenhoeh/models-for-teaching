import matplotlib.pyplot as plt
import toggleSwitch
import numpy as np
import scipy.integrate

a = 4
b = 4
n = 4
m = 4

x1 = np.linspace(0,a*1.05,100)
plt.plot(x1,toggleSwitch.activation(b,m,x1))

x2 = np.linspace(0,b*1.05,100)
plt.plot(toggleSwitch.activation(a,n,x2),x2)

st = toggleSwitch.steadyStates(a,b,n,m)
sty = [toggleSwitch.activation(b,m,x) for x in st]

if len(st) > 1:
    plt.plot(st[0],sty[0],marker='o',color='b')
    plt.plot(st[1],sty[1],marker='o',color='r')
    plt.plot(st[2],sty[2],marker='o',color='b')
else:
    plt.plot(st[0],sty[0],marker='o',color='r')


numTr = 50 # number of trajetories

T = np.linspace(0,50,1000)

for i in range(numTr):
    x0 = np.random.uniform(0,a*1.1)
    y0 = np.random.uniform(0,b*1.1)

    Y = scipy.integrate.odeint(toggleSwitch.deq,(x0,y0),T,args=(a,b,n,m))

    plt.plot(Y[:,0],Y[:,1],color='b')


plt.draw()
plt.show()
