import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
from scipy.integrate import ode


def modlg(t,x,r,d,c,K):
    g = r+d
    #x = y[0]
    dxdt = r*x*((g/r)*x/(x+c)-d/r-x/K)
    return np.array([dxdt])


tmax_sim = 100
tmax_plot = 30

ymax_plot = 40

deltat = 0.1

y0 = 1
r2 = 1
C2 = 1


def sim(x0, fn, *args):
    r = ode(fn)
    #print(args)
    r.set_initial_value(np.array(x0)).set_f_params(*args)
    Ylist = [r.y]
    Tlist = [r.t]
    while r.successful() and r.t<tmax_sim:
        Ylist.append(r.integrate(r.t+deltat))
        Tlist.append(r.t)
    Y = np.array(Ylist)
    T = np.array(Tlist)

    return T, Y


x0range = np.linspace(0,20,50)

def simall(fn, *args):
    Ys = []
    for x0 in x0range:
        T, Y = sim([x0], fn, *args)
        Ys.append(Y)
    return T, Ys


slider_height = 0.05
slider_gap = 0.15

slider_data = {
    'c': [0,12,5,0.01],
    }

vals = {}
for k,v in slider_data.items():
    vals[k] = v[2]


#fig, (ax1, ax2) = plt.subplots(1,2)
fig, ax = plt.subplots()
plt.subplots_adjust(left=0.1, right=0.9, bottom=len(slider_data)*slider_height + slider_gap)


T, Y = sim([10], modlg, 2, 1, vals['c'], 40)
T,Ys = simall(modlg, 2, 1, vals['c'], 40)

llist = []

for Y in Ys:
    l, = plt.plot(T, Y, 'b')
    llist.append(l)
#ax1.legend(['prey','predator'])
ax.set_xlim(0,tmax_plot)
ax.set_ylim(0,ymax_plot)
ax.margins(x=0)
ax.set_xlabel('time')


axcolor = 'lightgoldenrodyellow'
axlist = []
sliders = {}
y = slider_height
for k,v in slider_data.items():
    ax = plt.axes([0.25,y,0.65,slider_height*0.6], facecolor=axcolor)
    axlist.append(ax)
    y += slider_height

    sliders[k] = Slider(ax, k, v[0], v[1], valinit=v[2], valstep=v[3])

def update(val):
    for k,slider in sliders.items():
        vals[k] = slider.val
    T,Ys = simall(modlg, 2, 1, vals['c'], 40)

    for i in range(len(llist)):
        llist[i].set_xdata(T)
        llist[i].set_ydata(Ys[i])
    
    
for slider in sliders.values():
    slider.on_changed(update)

resetax = plt.axes([0.1, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')

def reset(event):
    for slider in sliders.values():
        slider.reset()

button.on_clicked(reset)


plt.show()
