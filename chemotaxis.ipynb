{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bacterial Chemotaxis\n",
    "\n",
    "A simple model of bacterial chemotaxis was presented in [Kollmann et al. (2005), Nature](https://doi.org/10.1038/nature04228). The model simulates the phosphorylation of the CheY protein in dependence on an external ligand concentration. The phosphorylation status of CheY determines the direction of the flagellar motor, and thus wether the bacterium is in 'swimming' or 'tumbling' mode. \n",
    "\n",
    "A key feature of the model (and the biological system) is its perfect adaptability. For constant ligand concentration, the CheY protein always reaches the same steady-state phosphorylation state. This is important, because changes in the motion should only be triggered upon changes in concentration. This remarkable feature leads to the ability to swim on average in the direction of nutrient gradient.\n",
    "\n",
    "The model contains three variables:\n",
    "\n",
    "$T_M$: the methylated receptor molecules\n",
    "$A_p$: the phosphorylated Che-A proteins\n",
    "$Y_p$: the phosphorylated Che-Y proteins\n",
    "\n",
    "A particular feature is that the activity of the receptor (activating Che-A) is dependent on the ligand concentration. This means that the receptor becomes less sensitive if ligand concentration is high. This active fraction $T_A$ is calculated as\n",
    "\n",
    "$T_A = p(L)T_M$ with $p(L) = V\\left(1-\\frac{L^H}{L^H+K^H}\\right),$\n",
    "\n",
    "where $L$ is the ligand concentration and $V$, $K$ and $H$ some constants.\n",
    "\n",
    "Only the active form $T_A$ of the receptor is i) subject to deactivation and ii) activating Che-A.\n",
    "\n",
    "The dynamics of the signalling cascade is described by the following system of differential equations:\n",
    "\n",
    "$\\frac{dT_M}{dt}=k_R R-k_B B^T\\frac{T_A}{K_B+T_A}\\\\\n",
    "\\frac{dAp}{dt}=k_AT_A(A^T-Ap)-k_YAp(Y^T-Yp)\\\\\n",
    "\\frac{dYp}{dt}=k_YAp(Y^T-Yp)-\\gamma_YYp.$\n",
    "    \n",
    "For the tutorial, use the following parameters:\n",
    "\n",
    "$K=V=1$mM, and $H=1.2$.\n",
    "\n",
    "Use the following concentrations (in $\\mu\\text{M}$): $R=0.16$, $A^T=5.3$, $B^T=0.28$, $Y^T=9.7$ and the rate\n",
    "constants $k_R=0.5$, $k_B=16$, $k_A=50$, $k_Y=100$ (in $\\mu\\text{M}^{-1}\\text{s}^{-1}$) and\n",
    "$\\gamma_Y=25\\text{s}^{-1}$ and the Michaelis constant $K_B=16\\mu\\text{M}$.\n",
    "Explore various time courses for the ligand concentration, e.g.\n",
    "\n",
    "$ L(t)=\\left\\{\n",
    "    \\begin{array}{c@{\\hspace{1em}}l}\n",
    "      1 & 100<t<200\\\\\n",
    "      0 & \\text{else}\n",
    "    \\end{array}\n",
    "  \\right.\n",
    "$\n",
    "\n",
    "and plot the concentration of phosphorylated CheY ($Yp$) as a function of\n",
    "time.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Could not load modelbase.assimulate. Sundials support disabled.\n"
     ]
    }
   ],
   "source": [
    "\"\"\"\n",
    "Import modelbase and useful modules\n",
    "\"\"\"\n",
    "\n",
    "import modelbase\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "Define model parameters as dictionary and chemical species as list\n",
    "\"\"\"\n",
    "\n",
    "par = {'K': 1,\n",
    "       'V': 1,\n",
    "       'H': 1.2,\n",
    "       'R': 0.16,\n",
    "       'AT': 5.3,\n",
    "       'BT': 0.28,\n",
    "       'YT': 9.7,\n",
    "       'kR': 0.5,\n",
    "       'kB': 16,\n",
    "       'kA': 50,\n",
    "       'kY': 100,\n",
    "       'gammaY': 25,\n",
    "       'KB': 16,\n",
    "       'L': 0,\n",
    "      }\n",
    "\n",
    "cpd_list = ['TM','Ap','Yp']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "Instatiate modelbase object, called 'm'\n",
    "\"\"\"\n",
    "\n",
    "m = modelbase.Model(par)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "Define the compound names\n",
    "\"\"\"\n",
    "\n",
    "m.set_cpds(cpd_list)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "Define the active receptors TA as an algebraic module:\n",
    "TA is derived from TM and the parameter L (and some others), but is used at various occurences. \n",
    "So it is useful to have access to this dependent variable.\n",
    "\"\"\"\n",
    "\n",
    "def TA(par,y):\n",
    "    L = par.L\n",
    "    H = par.H\n",
    "    p = par.V * (1 - L**H / (L**H + par.K**H))\n",
    "    return np.array([p * y[0]])\n",
    "\n",
    "m.add_algebraicModule(TA,'TA',['TM'],['TA'])\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "Now define all processes involved in the model:\n",
    "- Receptor methylation and demethylation ('meth' and 'demeth')\n",
    "- Activation of Che-A ('actA')\n",
    "- Conversion of Che-Ap into Che-Yp (a phosphate group is transferred - 'actY')\n",
    "- Deactivation of Che-Y ('deactY')\n",
    "\"\"\"\n",
    "\n",
    "\n",
    "m.add_reaction('meth', lambda p:p.kR * p.R, {'TM':1})\n",
    "\n",
    "def demeth(par,TA):\n",
    "    return par.kB * par.BT * TA/(par.KB + TA)\n",
    "\n",
    "m.add_reaction('demeth', demeth, {'TM':-1}, 'TA')\n",
    "\n",
    "def actA(par,TA,Ap):\n",
    "    return par.kA * TA * (par.AT - Ap)\n",
    "\n",
    "m.add_reaction('actA', actA, {'Ap':1}, 'TA', 'Ap')\n",
    "\n",
    "def actY(par,Ap,Yp):\n",
    "    return par.kY * Ap * (par.YT - Yp)\n",
    "\n",
    "m.add_reaction('actY', actY, {'Ap':-1,'Yp':1}, 'Ap', 'Yp')\n",
    "\n",
    "def deactY(par,Yp):\n",
    "    return par.gammaY * Yp\n",
    "\n",
    "m.add_reaction('deactY', deactY, {'Yp':-1}, 'Yp')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[0.58181778, 0.11291204, 3.01794001],\n",
       "       [0.51617979, 0.28755069, 5.19885995],\n",
       "       [0.46295349, 0.23497852, 4.70868274],\n",
       "       [0.4222028 , 0.20019589, 4.32098077],\n",
       "       [0.39104544, 0.17635796, 4.01846063],\n",
       "       [0.36724749, 0.15958227, 3.78429005],\n",
       "       [0.34908512, 0.14754189, 3.60393917],\n",
       "       [0.33523221, 0.13877279, 3.46550045],\n",
       "       [0.32467113, 0.13231614, 3.35947595],\n",
       "       [0.31662253, 0.1275232 , 3.27840628],\n",
       "       [0.31049034, 0.1239434 , 3.21648914],\n",
       "       [0.30581924, 0.12125732, 3.16923937],\n",
       "       [0.30226165, 0.11923482, 3.1332047 ],\n",
       "       [0.29955245, 0.11770794, 3.1057357 ],\n",
       "       [0.29748953, 0.11655293, 3.0848034 ],\n",
       "       [0.29591881, 0.11567789, 3.06885639],\n",
       "       [0.29472293, 0.11501421, 3.05670969],\n",
       "       [0.29381247, 0.11451039, 3.04745901],\n",
       "       [0.29311933, 0.11412768, 3.04041465],\n",
       "       [0.29259165, 0.11383681, 3.03505084],\n",
       "       [0.29218994, 0.11361566, 3.03096693],\n",
       "       [0.29188413, 0.11344746, 3.02785764],\n",
       "       [0.29165133, 0.11331952, 3.02549049],\n",
       "       [0.29147411, 0.11322218, 3.02368837],\n",
       "       [0.29133921, 0.11314811, 3.02231645],\n",
       "       [0.29123651, 0.11309175, 3.02127205],\n",
       "       [0.29115834, 0.11304885, 3.02047699],\n",
       "       [0.29109882, 0.1130162 , 3.01987174],\n",
       "       [0.29105352, 0.11299135, 3.019411  ],\n",
       "       [0.29101904, 0.11297244, 3.01906027],\n",
       "       [0.29099279, 0.11295804, 3.01879327],\n",
       "       [0.2909728 , 0.11294708, 3.01859002],\n",
       "       [0.29095759, 0.11293874, 3.0184353 ],\n",
       "       [0.29094601, 0.11293239, 3.01831752],\n",
       "       [0.29093719, 0.11292756, 3.01822787],\n",
       "       [0.29093048, 0.11292388, 3.01815962],\n",
       "       [0.29092538, 0.11292108, 3.01810766],\n",
       "       [0.29092149, 0.11291894, 3.01806811],\n",
       "       [0.29091853, 0.11291732, 3.01803801],\n",
       "       [0.29091627, 0.11291608, 3.01801509],\n",
       "       [0.29091456, 0.11291514, 3.01799764],\n",
       "       [0.29091325, 0.11291443, 3.01798436],\n",
       "       [0.29091226, 0.11291388, 3.01797425],\n",
       "       [0.2909115 , 0.11291347, 3.01796656],\n",
       "       [0.29091093, 0.11291315, 3.0179607 ],\n",
       "       [0.29091049, 0.11291291, 3.01795624],\n",
       "       [0.29091015, 0.11291273, 3.01795285],\n",
       "       [0.2909099 , 0.11291259, 3.01795026],\n",
       "       [0.29090971, 0.11291248, 3.0179483 ],\n",
       "       [0.29090956, 0.1129124 , 3.0179468 ],\n",
       "       [0.29090945, 0.11291234, 3.01794566],\n",
       "       [0.29090936, 0.1129123 , 3.01794479],\n",
       "       [0.2909093 , 0.11291226, 3.01794413],\n",
       "       [0.29090925, 0.11291223, 3.01794363],\n",
       "       [0.29090921, 0.11291221, 3.01794324],\n",
       "       [0.29090918, 0.1129122 , 3.01794295],\n",
       "       [0.29090916, 0.11291218, 3.01794273],\n",
       "       [0.29090914, 0.11291218, 3.01794257],\n",
       "       [0.29090913, 0.11291217, 3.01794244],\n",
       "       [0.29090912, 0.11291216, 3.01794234],\n",
       "       [0.29090911, 0.11291216, 3.01794227],\n",
       "       [0.29090911, 0.11291216, 3.01794221],\n",
       "       [0.2909091 , 0.11291215, 3.01794217],\n",
       "       [0.2909091 , 0.11291215, 3.01794213],\n",
       "       [0.2909091 , 0.11291215, 3.01794211],\n",
       "       [0.2909091 , 0.11291215, 3.01794209],\n",
       "       [0.2909091 , 0.11291215, 3.01794207],\n",
       "       [0.29090909, 0.11291215, 3.01794206],\n",
       "       [0.29090909, 0.11291215, 3.01794205],\n",
       "       [0.29090909, 0.11291215, 3.01794205],\n",
       "       [0.29090909, 0.11291215, 3.01794204],\n",
       "       [0.29090909, 0.11291215, 3.01794204],\n",
       "       [0.29090909, 0.11291215, 3.01794203],\n",
       "       [0.29090909, 0.11291215, 3.01794203],\n",
       "       [0.29090909, 0.11291215, 3.01794203],\n",
       "       [0.29090909, 0.11291215, 3.01794203],\n",
       "       [0.29090909, 0.11291215, 3.01794203],\n",
       "       [0.29090909, 0.11291215, 3.01794203],\n",
       "       [0.29090909, 0.11291215, 3.01794203],\n",
       "       [0.29090909, 0.11291215, 3.01794203],\n",
       "       [0.29090909, 0.11291215, 3.01794203],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202],\n",
       "       [0.29090909, 0.11291215, 3.01794202]])"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\"\"\"\n",
    "Perform a simulation, where L=0 for 100s, L=1 for 100s and L=0 for 100s.\n",
    "For this, create a Simulator instance.\n",
    "Run three simulations with 'timeCourse', between which the parameter L (ligand) is changed.\n",
    "\"\"\"\n",
    "\n",
    "y0 = np.zeros(3)\n",
    "s = modelbase.Simulator(m)\n",
    "T = np.linspace(0,100,100)\n",
    "s.timeCourse(T,y0)\n",
    "m.par.L=1\n",
    "s.timeCourse(T,None) # if initial values are 'None', the simulation continues from the last values.\n",
    "m.par.L=0\n",
    "s.timeCourse(T,None)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[<matplotlib.lines.Line2D at 0x7fed819eee48>]"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAWoAAAD4CAYAAADFAawfAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy8QZhcZAAAajklEQVR4nO3daXAc93nn8e8zBw4C4A1SJEWKh20x1GGRgbnysYqtlGxLTqL1lmsl2YmdlCuMFafiVCW7pWxqE29V8sLZbJzs2klKsbVS1o7s2LJjb6JIvpQoPkQF1AmKoniK4gmAJO5jMNPPvpgecAjiGJKY6e7B71PF0nBmBDzNBn548PS/u83dERGR+EpFXYCIiMxOQS0iEnMKahGRmFNQi4jEnIJaRCTmMtX4oCtXrvSNGzdW40OLiNSlPXv29Lp7+3SvVSWoN27cSGdnZzU+tIhIXTKz12d6TaMPEZGYU1CLiMScglpEJOYU1CIiMaegFhGJOQW1iEjMKahFRGJOQS2SUE90naZncDzqMqQGFNQiCTQ2UeATX9rDh//6mahLkRpQUIskUBDe8ONA91DElUgtKKhFEqgQ6M5MC4mCWiSBgiDqCqSWFNQiCVTQvU4XlIqunmdmR4FBoADk3b2jmkWJyOzyaqkXlMu5zOl73L23apWISMWU0wuLRh8iCaTRx8JSaVA78B0z22Nmu6Z7g5ntMrNOM+vs6emZvwpF5BKBVn0sKJUG9bvcfQdwJ/BJM7tt6hvc/UF373D3jvb2ae8mIyLzpHx5nqu7rnsVBbW7nwj/2w18E9hZzaJEZHblo4+xCQ2s692cQW1mLWbWVnoMvBfoqnZhIjKz8o56cHwiwkqkFipZ9bEa+KaZld7/t+7+RFWrEpFZlQf10FieVW0RFiNVN2dQu/th4K01qEVEKnRRRz2Wj7ASqQUtzxNJoKBsRj00rqCudwpqkQRSR72wKKhFEkgd9cKioBZJoHyh/GCiVn3UOwW1SAKVr6PW6KP+KahFEqj8okyDGn3UPQW1SAKVd9R9I7kIK5FaUFCLJFD5RZn6RjSjrncKapEEKi3PW9SQpm9UQV3vFNQiCZQPg3pFawP96qjrnoJaJIFK66iXtzTSr4667imoRRKoNPpYvihL36gOJtY7BbVIApV31GMTAWMThYgrkmpSUIskUKFsRg1o/FHnFNQiCTQ5+mgpBrWW6NU3BbVIAl2YUaujXggU1CIJVDozsTT60NmJ9U1BLZJAwdTRhzrquqagFkmgyYOJLY0AOumlzimoRRKodDnqxc0Z0inTWuo6p6AWSaDS6COdMpa3NHB2SEFdzxTUIglUOpiYThkrWxvpHRqPuCKpJgW1SAIVgvKgbqBHHXVdU1CLJNBkUFvYUQ+qo65nCmqRBJraUfcOjeNld32R+qKgFkmgwB0zsLCjHs8HDOd0YaZ6paAWSaBC4KTNAFjZWlxLrfFH/VJQiyRQwZ1UKgzqtjCotfKjbimoRRKoUHAyYVCvCE8jV1DXr4qD2szSZva8mf1DNQsSkbkV/MLooz3sqLVEr35dTkf9KWBftQoRkcoFwYXRR+nCTGfVUdetioLazK4FPgB8obrliEglCu6kw6DOplOsaGngzMBYxFVJtVTaUf8Z8F+AYKY3mNkuM+s0s86enp55KU5EplcIIBWOPgDWLG3iZJ+Cul7NGdRm9nNAt7vvme197v6gu3e4e0d7e/u8FSgilwoCJ1323bt2STOn+kejK0iqqpKO+p3AL5jZUeArwO1m9qWqViUis8oHTiZ14dt37dJmddR1bM6gdvffdfdr3X0jcC/wA3f/xapXJiIzCtxJlXfUS5sYGs8zMKYbCNQjraMWSaDyMxMB1ixpBuBkn8Yf9eiygtrd/9ndf65axYhIZcrPTITi6APglMYfdUkdtUgCBVM66rVLmwA4oY66LimoRRKoEFxYRw2wqq2JdMo0+qhTCmqRBCoEftE66nTKWLe0mTfOK6jrkYJaJIEK7mTSdtFzm1a2cLhnKKKKpJoU1CIJNLWjhmJQH+kd1p1e6pCCWiSBAr94Rg2wpb2FkVyBbt1AoO4oqEUSaOo6aoBNK1sBONwzHEVJUkUKapEECgIuOjMRYFN7CwCHezWnrjcKapEEygfBJaOPNYubaMqm1FHXIQW1SAIVHNJTWupUynjL6jZePT0QUVVSLQpqkQQqnpl46fM3rF1C14kBrfyoMwpqkQSaemZiyQ1rF9M/OqFTyeuMglokgQK/dB01wI3rlgDQdULjj3qioBZJoJk66q3XtJFOGV0n+iOoSqpFQS2SQIXg4sucljRl01y/uo09r5+PoCqpFgW1SAIV3MlME9QAt25ewZ5j5xmbKNS4KqkWBbVIAk13ZmLJ27esIJcPeP5YX42rkmpRUIskUDDD6ANg56blpAx+cvhsjauSalFQiyRQwWfuqJc0Z7n52qU89Wp3jauSalFQiyRQIWDGjhrgrpuu4eUT/Rw7O1LDqqRaFNQiCVQIAtKzfPfeeeMaAB7vOlWjiqSaFNQiCVQInMzUy+eVWb98Eds3LOVrnW8QBDqdPOkU1CIJFDjTnplY7qNvv45DPcM8faCnRlVJtSioRRKoeGbi7O/5wE1raW9r5C//+ZAu0pRwCmqRBCr4zMvzShoyKX7z9jex+8g5vvPKmRpVJtWgoBZJoGCWE17K3bdzA9evbuP3v9XF2SHdSzGpFNQiCZSf4aJMU2XSKT57zy2cH5ngk3/7nE4rTygFtUjClFZxzHUwsWTb2sX8jw/dzO4j5/jYQ8/Sq846cRTUIglTCA8MznRRpuncfcs6/uyeW3jhjT7e+9mnefhHRxgYm6hWiTLPMnO9wcyagKeBxvD9X3f3P6h2YSIyvUKpo76MoIZiWG+9ZjH/7e+7+PT/e4XPPLGfO7at5m2blnPzuiVcu6yZ5S0NWIWdutTOnEENjAO3u/uQmWWBH5rZP7n7M1Wu7aoVAqd7cIyTfWP0DI4xPF5gZKLAaC7PaC6gEAQEXuxQAneCwIt/Dxx3p9oLmpKyYmpLewu//M5NUZdBEDiDY3mGcnmGxvIMjRf/jOYKTBSCsj9+0eN8obh/S/vUHZzivi49dqf4uoND+P7i552vr4X52t/5MKgrmVFPdf01bXz1127lpeP9PPrsMb7/ajfffvHk5OuNmRSrFzfR0phhUUOaRQ1pmrNp0ikjlTLSZqSMssfF568226/2R8PVf/75+eHU0pjhgTu3zsvHKjdnUHtxAeZQ+Nds+CeWEZMvBPzLaz18b183zx87z4HuocnuYyYpK37Bp8IvunT4RZcKvyCrLe7dy0guz9hEwMfesbEmtXYPjrH/9CCHuoc41DPMib5RegbH6R4co3coN+f+nItZMRRSZuHj4hOp8HFp3xsQvoTN89fCfPw7trc1csPaxVf8+d+6filvXb8Ud+f4+VFeOTXAyb5RTvaN0j04zkiuwEguz+BYnp7BcQqBUwh/eBUCn2xmCu4Ugqvdmqvbp1f7A3A+w2xFS0M0QQ1gZmlgD/Am4PPuvnua9+wCdgFs2LBhPmuc09hEgS/+8AgP//goPYPjtDVl2L5hGbdvXcW6Zc2sXdJMe1sjbU0ZmhvSLGrITHYJMrvP/eAAf/Kd15goOA2Z+f/3Ojec43v7zvDMobN0vn6eY+cuXESorSnD+mWLWLW4kZ9a00Z7WyPLFjXQ1pShtTFLS2Oa1sbiPm3MpMikUmQzKbJpoyGdIpMuPs6kUsUgjvkPxSiYGeuXL2L98kVRlyKzqCio3b0A3GJmS4FvmtmN7t415T0PAg8CdHR01KzjPtg9xK9/eQ+vnRni3de38+GdG3jP1lVk5zptSyqSCf8d80FAwzwdey4EzlOvdvM3z7zOjw72Ugicla0NdFy3nI++/TpuWLuELataaG9tVLiKUGFQl7h7n5k9Bbwf6Jrr/dX2yskBPvKFZ0iZ8X9+5W285/pVUZdUd0o/8CYKV/+z1915cu9pPvPEfo70DrNmSROf+JnN3HnjGm5Yu1ihLDKDSlZ9tAMTYUg3A3cAn6l6ZXPoHRrn44/8G03ZNI/+6q1sXNkSdUl1KZsuhufEVQ4i3zg3wu987UV2HznHW1a38rkPb+d9N1yj33xEKlBJR70GeCScU6eAv3P3f6huWXN74LGXODec47H736GQrqLSpTTzV9FRP7n3NP/5ay/iDn/0wRu5p2P95EhFROZWyaqPl4DtNailYj949Qzf29fNf71rKzeuWxJ1OXUtc5Ud9SM/PsoffHsvN1+7hM/dt4MNK3TQSuRyXdaMOg7cnT9+Yj+bV7bwy++Ifm1vvWuYPJh4+R31F/71MH/4j/u4Y9tqPvfh7TRm0vNdnsiCkLjfP394sJdXTw9y/7u30JBJXPmJc6Ud9bdeOMEf/uM+7rrpGv7iIzsU0iJXIXFJ9/CPjtLe1sgv3LI26lIWhNKM+nKCev/pQR547GU6rlvGn9+7XQcMRa5Sor6Dzg/n+JfXeviPO9apQ6uR0qqPSg8mDo3nuf9Le2hpzPAXH9mhkBaZB4maUT+59zT5wPn5m9VN10q27ISXSvzJk/s5cnaYR3/1VlYtbqpmaSILRqLance7TrNxxaIrvsaBXL7SjDqXn7ujfvl4P4/85CgfvfU6bt28osqViSwciQnq8XyB3YfP8p6tq3QGWw1V2lG7O3/0+CssX9TAb7/v+lqUJrJgJCaonz/Wx3g+4B1bVkZdyoJSujj9XDPqpw/08szhc/zmz76ZxU3ZWpQmsmAkJqh/fOgsKYOdm5ZHXcqCcuFaHzN31O7O//7+AdYuaeK+nbW9cqLIQpCYoH7m0FluWreEJc3q1mqpkosydb5+ns7Xz7Prts1a2y5SBYn4rioETtfJfrZvWBZ1KQtO6WDibDPqh398lMVNGe55m7ppkWpIRFAf6R1mJFfQao8IZFOzd9TdA2M82XWa/9SxnuYGrW0XqYZEBPXek/0A3HStLsBUa5Md9Qwz6seeO0E+cD5y63W1LEtkQUlEUHed6Kcxk+JN7a1Rl7LgzHYw0d35xnPH+enrlrFJl5oVqZqEBPUAW9cs1jWMI3DhxgGXjj72nhzgQPcQH9y+rtZliSwoiUi+A91DXL9a3XQUMrOc8PL4y6dIp4wP3LSm1mWJLCixD+rBsQl6h8bZrLFHJEonvEzXUT+59zT/btNylrU01LoskQUl9kF9tHcEQDPQiMw0oz7YPcihnmHed8M1UZQlsqDEPqgP9w4BCuqopFNGyi49hfz7+7oBuGPb6ijKEllQYh/UR3qHMYMNy3Wvvahk0ikmpsyo//VAL29Z3crapc0RVSWycCQiqNctbaYpq5MpopJN2UUd9WiuwLNHz3Hbm9sjrEpk4UhEUGvsEa1sJnXRCS+7j5wllw/4929RUIvUQuyD+vj5UdZr7BGpTCpFrqyjfubwObJpY+dGXclQpBZiHdRjEwXODedYo1s6RSqbtos66mePFK9kqGt7iNRGrIP6dP8YAGt0wCpSmbSRD4od9WiuwMsn+tm5SbfaEqmVWAf1yf5RANYsUUcdpWw6NbmO+vk3zjNRcHZu0iVnRWol1kE92VErqCOVTV0I6udePw/AT2/QfFqkVmId1Kcmg1qjjyhl0heW5714vJ/NK1tYskh32hGplVgH9cm+UZYuyuqgVcSKJ7wUg/ql433crOuCi9TUnEFtZuvN7Ckze8XM9prZp2pRGBRHH+qmo9cQrvo43T/GmYFxbr52adQliSwomQrekwd+292fM7M2YI+ZfdfdX6lybZzsH9N8OgYy4Yz6xeN9ALx1vYJapJbm7Kjd/ZS7Pxc+HgT2ATW5UnzP4Dir2hpr8alkFpm0MVFw9p4cIGWwbY3uXSlSS5c1ozazjcB2YPc0r+0ys04z6+zp6bnqwoLAOTc8zspWBXXUsukU+SBg36kBNq1s0TEDkRqrOKjNrBV4DPgtdx+Y+rq7P+juHe7e0d5+9deA6BudIHBYrovSRy4brvp49fQAP6VuWqTmKgpqM8tSDOkvu/s3qltS0bnhcQBWtCqoo5ZJpzg/kuONc6MKapEIVLLqw4AvAvvc/U+rX1JR71AOgBUtGn1ELZsyzgwUf3BuvaYt4mpEFp5KOup3Ar8E3G5mL4R/7qpyXZwbDoNaHXXkyu/+vlUdtUjNzbk8z91/CFgNarnI2SGNPuIimy7u/tbGDGu1XFKk5mJ7ZuLZsKNetkhBHbXSDW63tLdQnISJSC3FN6iHcixdlJ0MCYlOJlUK6taIKxFZmGKbgmeHx1mhpXmxkCsUANiySkEtEoX4BvVQTis+YuLYueJ1wdVRi0QjvkE9nNOBxJg4dnYYgM3tusmwSBRiG9R9IzmW6kBiLNzztg0AbNBNhkUiUcnV82rO3RkYzbOkWRenj4P7372FT/zMZq34EIlILDvq8XxArhCwuDmWP0cWJIW0SHRiGdT9oxMA6qhFRIhpUA+EQb24SUEtIhLPoB4Lg1odtYhIPINaow8RkQtiGdQDo3kAFjfpYKKISDyDWqMPEZFJsQzq/hEdTBQRKYllUA+MTdCcTdOQiWV5IiI1Fcsk1FmJIiIXxDKo+0cndFaiiEgolkE9MDah+bSISCi2Qa3Rh4hIUSyDujj6UFCLiEBMg3pwLE+bTnYREQFiGtQj4wVaGhXUIiIQw6DOhdeibmlIR12KiEgsxC6oR3PFO14valBHLSICMQzq4VzxgkwtjeqoRUQgjkE9XgxqddQiIkXxC+pw9NGqg4kiIkAMg3pksqPW6ENEBCoIajN7yMy6zayrFgWVOmotzxMRKaqko34YeH+V65g0rI5aROQicwa1uz8NnKtBLcCFVR+aUYuIFM3bjNrMdplZp5l19vT0XPHHGRkP11ErqEVEgHkMand/0N073L2jvb39ij9OqaNuzmr0ISICcVz1kSvQnE2TTlnUpYiIxELsgnpoPK8VHyIiZSpZnvco8BPgejM7bmYfr2ZBI+N5nT4uIlJmztbV3e+rRSElw7mCTh8XESkTu9HHSC6vS5yKiJSJXVAPjxe0NE9EpEwMgzpPq2bUIiKTYhfUI5pRi4hcJHZBPZzL6zofIiJlYhfUo7kCzQpqEZFJsQrqIHDG84FOHxcRKROroB7PBwA0KahFRCbFKqhHJ4pXzlNHLSJygYJaRCTmYhXUY2FQN2ZjVZaISKRilYijOXXUIiJTxSqoSx21DiaKiFwQs6AurvrQOmoRkQtiFdQ6mCgicqlYBnWTDiaKiEyKVSJqRi0icikFtYhIzMUyqDWjFhG5IFZBPZrTtT5ERKaKV1BPFGhIp0inLOpSRERiI1ZBPTZR0IoPEZEpYpWKxaDW2ENEpFysgnp0Qnd3ERGZKlZBPTZR0IoPEZEpYhXUoxMBjQpqEZGLxCqox3IFmnUwUUTkIrFKxbG8DiaKiEwVq6AezWlGLSIyVUVBbWbvN7P9ZnbQzB6oVjFjeQW1iMhUcwa1maWBzwN3AtuA+8xsWzWKGc3pYKKIyFSVdNQ7gYPuftjdc8BXgLurUYyW54mIXKqSoF4HvFH29+Phcxcxs11m1mlmnT09PVdUzB3bVnPjusVX9P+KiNSrzHx9IHd/EHgQoKOjw6/kY3z2nlvmqxwRkbpRSUd9Alhf9vdrw+dERKQGKgnqfwPebGabzKwBuBf4dnXLEhGRkjlHH+6eN7PfAJ4E0sBD7r636pWJiAhQ4Yza3R8HHq9yLSIiMo1YnZkoIiKXUlCLiMScglpEJOYU1CIiMWfuV3Ruyuwf1KwHeP0K//eVQO88lhOletmWetkO0LbEUb1sB1zdtlzn7u3TvVCVoL4aZtbp7h1R1zEf6mVb6mU7QNsSR/WyHVC9bdHoQ0Qk5hTUIiIxF8egfjDqAuZRvWxLvWwHaFviqF62A6q0LbGbUYuIyMXi2FGLiEgZBbWISMzFJqhrdQPdajGzo2b2spm9YGad4XPLzey7ZnYg/O+yqOucjpk9ZGbdZtZV9ty0tVvR/wr300tmtiO6yi81w7Z82sxOhPvmBTO7q+y13w23Zb+ZvS+aqi9lZuvN7Ckze8XM9prZp8LnE7dfZtmWJO6XJjN71sxeDLflv4fPbzKz3WHNXw0vCY2ZNYZ/Pxi+vvGKPrG7R/6H4uVTDwGbgQbgRWBb1HVd5jYcBVZOee6PgQfCxw8An4m6zhlqvw3YAXTNVTtwF/BPgAG3Arujrr+Cbfk08DvTvHdb+LXWCGwKvwbTUW9DWNsaYEf4uA14Law3cftllm1J4n4xoDV8nAV2h//efwfcGz7/V8D94eNfB/4qfHwv8NUr+bxx6ahrdgPdGrsbeCR8/AjwHyKsZUbu/jRwbsrTM9V+N/A3XvQMsNTM1tSm0rnNsC0zuRv4iruPu/sR4CDFr8XIufspd38ufDwI7KN4r9LE7ZdZtmUmcd4v7u5D4V+z4R8Hbge+Hj4/db+U9tfXgZ81M7vczxuXoK7oBrox58B3zGyPme0Kn1vt7qfCx6eB1dGUdkVmqj2p++o3wpHAQ2UjqERsS/jr8naK3Vui98uUbYEE7hczS5vZC0A38F2KHX+fu+fDt5TXO7kt4ev9wIrL/ZxxCep68C533wHcCXzSzG4rf9GLv/skci1kkmsP/SWwBbgFOAX8z2jLqZyZtQKPAb/l7gPlryVtv0yzLYncL+5ecPdbKN4/diewtdqfMy5Bnfgb6Lr7ifC/3cA3Ke7AM6VfP8P/dkdX4WWbqfbE7St3PxN+cwXAX3Ph1+hYb4uZZSkG25fd/Rvh04ncL9NtS1L3S4m79wFPAW+nOGoq3TGrvN7JbQlfXwKcvdzPFZegTvQNdM2sxczaSo+B9wJdFLfhY+HbPgZ8K5oKr8hMtX8b+Gi4yuBWoL/sV/FYmjKr/SDFfQPFbbk3PDK/CXgz8Gyt65tOOMf8IrDP3f+07KXE7ZeZtiWh+6XdzJaGj5uBOyjO3J8CPhS+bep+Ke2vDwE/CH8TujxRH0UtO5p6F8WjwYeA34u6nsusfTPFo9QvAntL9VOcRX0fOAB8D1geda0z1P8oxV89JyjO1z4+U+0Uj3p/PtxPLwMdUddfwbb837DWl8JvnDVl7/+9cFv2A3dGXX9ZXe+iONZ4CXgh/HNXEvfLLNuSxP1yM/B8WHMX8Pvh85sp/jA5CHwNaAyfbwr/fjB8ffOVfF6dQi4iEnNxGX2IiMgMFNQiIjGnoBYRiTkFtYhIzCmoRURiTkEtIhJzCmoRkZj7/43V6shr1MIMAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "\"\"\"\n",
    "Plot phosphorylated Che-Y (Yp) as a function of time\n",
    "\"\"\"\n",
    "\n",
    "plt.plot(s.getT(),s.getVarByName('Yp'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
