import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
from scipy.integrate import ode

import toggleSwitch


axmax = 4.2
numTr = 20
x0 = np.random.uniform(0,axmax,(numTr,))
y0 = np.random.uniform(0,axmax,(numTr,))
tmax = 100
deltat = 0.1


def simToggles(a,b,n,m):
    r = ode(toggleSwitch.toggleswitch_ode)
    T = []
    Y = []
    for i in range(numTr):
        r.set_initial_value(np.array([x0[i],y0[i]])).set_f_params(a,b,n,m)
        Ylist = [r.y]
        Tlist = [r.t]
        while r.successful() and r.t<tmax:
            Ylist.append(r.integrate(r.t+deltat))
            Tlist.append(r.t)
        Y.append(np.array(Ylist))
        T.append(np.array(Tlist))

    return T, Y



fig, ax = plt.subplots()
plt.subplots_adjust(left=0.1, bottom=0.3)

a0 = 3
b0 = 3
n0 = 3
m0 = 3
delta_a = 0.05
delta_b = 0.05
delta_n = 0.1
delta_m = 0.1


T, Y = simToggles(a0,b0,n0,m0)

lines = []
for i in range(numTr):
    l, = plt.plot(Y[i][:,0],Y[i][:,1],'k')
    lines.append(l)
                       
#l, = plt.plot(Y[:,0], Y[:,1])
ax.set_xlim(0,4.2)
ax.set_ylim(0,4.2)
ax.margins(x=0)

# nullclines
x1 = np.linspace(0,4.2,100)
y1 = toggleSwitch.activation(b0,m0,x1)
nc1, = plt.plot(x1,y1,'b')

y2 = np.linspace(0,4.2,100)
x2 = toggleSwitch.activation(a0,n0,y2)
nc2, = plt.plot(x2,y2,'r')




axcolor = 'lightgoldenrodyellow'
axa = plt.axes([0.25, 0.05, 0.65, 0.03], facecolor=axcolor)
axb = plt.axes([0.25, 0.10, 0.65, 0.03], facecolor=axcolor)
axn = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)
axm = plt.axes([0.25, 0.20, 0.65, 0.03], facecolor=axcolor)
#axamp = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)

slidera = Slider(axa, 'a', 2, 4, valinit=a0, valstep=delta_a)
sliderb = Slider(axb, 'b', 2, 4, valinit=b0, valstep=delta_b)
slidern = Slider(axn, 'n', 1, 5, valinit=n0, valstep=delta_n)
sliderm = Slider(axm, 'm', 1, 5, valinit=m0, valstep=delta_m)
#samp = Slider(axamp, 'Amp', 0.1, 10.0, valinit=a0)


def update(val):
    a = slidera.val
    b = sliderb.val
    n = slidern.val
    m = sliderm.val
    
    T, Y = simToggles(a,b,n,m)
    for i in range(numTr):
        lines[i].set_xdata(Y[i][:,0])
        lines[i].set_ydata(Y[i][:,1])

    y1 = toggleSwitch.activation(b,m,x1)
    nc1.set_ydata(y1)
    x2 = toggleSwitch.activation(a,n,y2)
    nc2.set_xdata(x2)
    fig.canvas.draw_idle()


slidera.on_changed(update)
sliderb.on_changed(update)
slidern.on_changed(update)
sliderm.on_changed(update)

resetax = plt.axes([0.1, 0.05, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')


def reset(event):
    slidera.reset()
    sliderb.reset()
    slidern.reset()
    sliderm.reset()
button.on_clicked(reset)

"""
rax = plt.axes([0.025, 0.5, 0.15, 0.15], facecolor=axcolor)
radio = RadioButtons(rax, ('red', 'blue', 'green'), active=0)


def colorfunc(label):
    l.set_color(label)
    fig.canvas.draw_idle()
radio.on_clicked(colorfunc)

# Initialize plot with correct initial active value
colorfunc(radio.value_selected)
"""

plt.show()

